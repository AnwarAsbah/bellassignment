package com.anwarissaasbah.bell.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    @JvmStatic
    fun getCorrectedDateString(dateS: String): String {
        return try {
            val date = parse(dateS)
            date?.let { format(it) } ?: ""
        } catch (e: Exception) {
            ""
        }
    }

    @JvmStatic
    fun parse(dateS: String): Date? {
        try {
            val formatter = SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH)
            return formatter.parse(dateS)
        } catch (e: Exception) {
            return null
        }
    }

    @JvmStatic
    private fun format(date: Date): String? {
        try {
            val formatter = SimpleDateFormat("MMM dd, yyyy hh:mm aaa", Locale.ENGLISH)
            return formatter.format(date)
        } catch (e: Exception) {
            return null
        }
    }
}