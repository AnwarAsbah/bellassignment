package com.anwarissaasbah.bell.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.text.Spannable
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.View
import android.view.animation.TranslateAnimation

object UiUtils {

    @JvmStatic
    fun slideUp(view: View) {
        view.visibility = View.VISIBLE
        val animate = TranslateAnimation(0f, 0f, view.height.toFloat(), 0f)
        animate.duration = 300
        animate.fillAfter = true
        view.startAnimation(animate)
    }

    @JvmStatic
    fun slideDown(view: View) {
        val animate = TranslateAnimation(0f, 0f, 0f, view.height.toFloat())
        animate.duration = 300
        animate.fillAfter = true
        view.startAnimation(animate)
    }

    @JvmStatic
    fun setHyperLink(context: Context, spannable: Spannable, urlString: String) {

        val msg = spannable.toString()
        if (msg.contains(urlString, true)) {
            val startIndex = msg.indexOf(urlString, 0, true)
            val endIndex = startIndex + urlString.length
            spannable.setSpan(
                object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(urlString))
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        try {
                            context.startActivity(intent)
                        } catch (ex: ActivityNotFoundException) {
                        }
                    }
                },
                startIndex,
                endIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            spannable.setSpan(
                ForegroundColorSpan(Color.BLUE),
                startIndex,
                endIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            spannable.setSpan(
                UnderlineSpan(),
                startIndex,
                endIndex,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
        }
    }
}