package com.anwarissaasbah.bell.data

import com.fasterxml.jackson.annotation.*
import java.io.Serializable
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class Tweet : Serializable {

    @JsonProperty("created_at")
    @get:JsonProperty("created_at")
    @set:JsonProperty("created_at")
    var createdAt: String? = null
    @JsonProperty("id")
    @get:JsonProperty("id")
    @set:JsonProperty("id")
    var id: Long = 0
    @JsonProperty("id_str")
    @get:JsonProperty("id_str")
    @set:JsonProperty("id_str")
    var idStr: String? = null
    @JsonProperty("text")
    @get:JsonProperty("text")
    @set:JsonProperty("text")
    var text: String? = null
    @JsonProperty("source")
    @get:JsonProperty("source")
    @set:JsonProperty("source")
    var source: String? = null
    @JsonProperty("truncated")
    @get:JsonProperty("truncated")
    @set:JsonProperty("truncated")
    var isTruncated: Boolean = false
    @JsonProperty("in_reply_to_status_id")
    @get:JsonProperty("in_reply_to_status_id")
    @set:JsonProperty("in_reply_to_status_id")
    var inReplyToStatusId: Any? = null
    @JsonProperty("in_reply_to_status_id_str")
    @get:JsonProperty("in_reply_to_status_id_str")
    @set:JsonProperty("in_reply_to_status_id_str")
    var inReplyToStatusIdStr: Any? = null
    @JsonProperty("in_reply_to_user_id")
    @get:JsonProperty("in_reply_to_user_id")
    @set:JsonProperty("in_reply_to_user_id")
    var inReplyToUserId: Any? = null
    @JsonProperty("in_reply_to_user_id_str")
    @get:JsonProperty("in_reply_to_user_id_str")
    @set:JsonProperty("in_reply_to_user_id_str")
    var inReplyToUserIdStr: Any? = null
    @JsonProperty("in_reply_to_screen_name")
    @get:JsonProperty("in_reply_to_screen_name")
    @set:JsonProperty("in_reply_to_screen_name")
    var inReplyToScreenName: Any? = null
    @JsonProperty("user")
    @get:JsonProperty("user")
    @set:JsonProperty("user")
    var user: User? = null
    @JsonProperty("geo")
    @get:JsonProperty("geo")
    @set:JsonProperty("geo")
    var geo: Geo? = null
    @JsonProperty("coordinates")
    @get:JsonProperty("coordinates")
    @set:JsonProperty("coordinates")
    var coordinates: Coordinates? = null
    @JsonProperty("place")
    @get:JsonProperty("place")
    @set:JsonProperty("place")
    var place: Place? = null
    @JsonProperty("contributors")
    @get:JsonProperty("contributors")
    @set:JsonProperty("contributors")
    var contributors: Any? = null
    @JsonProperty("is_quote_status")
    @get:JsonProperty("is_quote_status")
    @set:JsonProperty("is_quote_status")
    var isIsQuoteStatus: Boolean = false
    @JsonProperty("quote_count")
    @get:JsonProperty("quote_count")
    @set:JsonProperty("quote_count")
    var quoteCount: Int = 0
    @JsonProperty("reply_count")
    @get:JsonProperty("reply_count")
    @set:JsonProperty("reply_count")
    var replyCount: Int = 0
    @JsonProperty("retweet_count")
    @get:JsonProperty("retweet_count")
    @set:JsonProperty("retweet_count")
    var retweetCount: Int = 0
    @JsonProperty("favorite_count")
    @get:JsonProperty("favorite_count")
    @set:JsonProperty("favorite_count")
    var favoriteCount: Int = 0
    @JsonProperty("entities")
    @get:JsonProperty("entities")
    @set:JsonProperty("entities")
    var entities: Entities? = null
    @JsonProperty("favorited")
    @get:JsonProperty("favorited")
    @set:JsonProperty("favorited")
    var isFavorited: Boolean = false
    @JsonProperty("retweeted")
    @get:JsonProperty("retweeted")
    @set:JsonProperty("retweeted")
    var isRetweeted: Boolean = false
    @JsonProperty("possibly_sensitive")
    @get:JsonProperty("possibly_sensitive")
    @set:JsonProperty("possibly_sensitive")
    var isPossiblySensitive: Boolean = false
    @JsonProperty("filter_level")
    @get:JsonProperty("filter_level")
    @set:JsonProperty("filter_level")
    var filterLevel: String? = null
    @JsonProperty("lang")
    @get:JsonProperty("lang")
    @set:JsonProperty("lang")
    var lang: String? = null
    @JsonProperty("matching_rules")
    @get:JsonProperty("matching_rules")
    @set:JsonProperty("matching_rules")
    var matchingRules: List<MatchingRule>? = null
    @JsonProperty("extended_tweet")
    @get:JsonProperty("extended_tweet")
    @set:JsonProperty("extended_tweet")
    var extendedTweet: ExtendedTweet? = null

    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}
