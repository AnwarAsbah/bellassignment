package com.anwarissaasbah.bell.modules.splash

import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.doOnEnd
import com.anwarissaasbah.bell.BaseActivity
import com.anwarissaasbah.bell.R
import com.anwarissaasbah.bell.modules.login.TwitterLoginActivity
import com.anwarissaasbah.bell.modules.map.TwitterMapActivity
import com.twitter.sdk.android.core.TwitterCore

private const val FADE_IN_AND_OUT_PERIOD: Long = 700
private const val WAIT_PERIOD: Long = 1000

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        init()
    }

    private fun init() {
        val splashImage = findViewById<ImageView>(R.id.splash_image)

        val fadeIn = ObjectAnimator.ofFloat(splashImage, View.ALPHA, 0f, 1f)
        fadeIn.duration =
            FADE_IN_AND_OUT_PERIOD
        fadeIn.doOnEnd {
            val fadeOut = ObjectAnimator.ofFloat(splashImage, View.ALPHA, 1f, 0f)
            fadeOut.duration =
                FADE_IN_AND_OUT_PERIOD
            fadeOut.startDelay = WAIT_PERIOD
            fadeOut.doOnEnd {
                startLoginOrMapActivity()
            }
            fadeOut.start()
        }

        fadeIn.start()

    }

    private fun startLoginOrMapActivity() {
        val session = TwitterCore.getInstance().sessionManager.activeSession

        if (session != null) { //if app has an active session navigate to map
            val authToken = session.authToken
            if (authToken?.token != null && authToken.secret != null) {
                startActivity(Intent(this, TwitterMapActivity::class.java))
                finish()
                return
            }
        }

        //no active session navigate to login
        startActivity(Intent(this, TwitterLoginActivity::class.java))
        finish()
    }

}
