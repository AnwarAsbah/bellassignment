package com.anwarissaasbah.bell.modules.map

import TweetData
import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.anwarissaasbah.bell.BaseActivity
import com.anwarissaasbah.bell.R
import com.anwarissaasbah.bell.data.Tweet
import com.anwarissaasbah.bell.modules.tweet.EmptyMapMarkerPopup
import com.anwarissaasbah.bell.modules.tweet.TweetDetailedActivity
import com.anwarissaasbah.bell.utils.DateUtils
import com.anwarissaasbah.bell.utils.UiUtils
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.concurrent.fixedRateTimer
import kotlin.math.ln
import kotlin.random.Random

private const val PERMISSION_ID = 1
private const val MAX_ALLOWED_DISTANCE: Double = 10000.0 //more than enough to cover the whole world
private const val DEFAULT_DISTANCE: Double = 5.0
private const val MAX_ALLOWED_PINS: Int = 100
private const val POLLING_PERIOD: Long = 10000L

class TwitterMapActivity : BaseActivity(), OnMapReadyCallback,
    TwitterMapPresenter.TwitterMapView {

    private lateinit var searchEditText: EditText
    private lateinit var searchDistanceEditText: EditText
    private lateinit var searchButton: Button
    private lateinit var moreButton: Button
    private lateinit var tweetShortText: TextView
    private lateinit var tweetCreateDate: TextView
    private lateinit var tweetPopup: View
    private lateinit var loadingIndicator: View
    private lateinit var langChangeButton: Button

    private var query: String = ""
    private var radius: Double = DEFAULT_DISTANCE
    private var animateCamera: Boolean = true

    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            requestTweets(mLastLocation.latitude, mLastLocation.longitude)
        }
    }

    private lateinit var mMap: GoogleMap
    private val markerTweetMap: HashMap<Marker, Tweet> = HashMap()
    private val tweetIDList: LinkedList<String> = LinkedList()
    private var mCircle: Circle? = null
    private var prevSelectedMarker: Marker? = null
    private var selectedTweet: Tweet? = null
    private var isPopupDisplayed = false

    private var presenter: TwitterMapPresenter = TwitterMapPresenter()
    private var timer: Timer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_twitter_map)
        initMap()
    }

    override fun onResume() {
        super.onResume()
        startPolling()
    }

    override fun onPause() {
        super.onPause()
        pausePolling()
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        initView()
        mMap = googleMap
        mMap.setInfoWindowAdapter(EmptyMapMarkerPopup(this)) //to keep same behavior by make it invisible
        mMap.setOnMarkerClickListener { marker ->
            onMapMarkerClicked(marker)
            false
        }
        mMap.setOnInfoWindowCloseListener { marker ->
            onMarkerInfoWindowClosed(marker)
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        startPolling()
    }

    private fun startPolling() {
        pausePolling() //pause prev if any
        if (::mMap.isInitialized && ::mFusedLocationClient.isInitialized) {
            timer = fixedRateTimer("polling", false, 0, POLLING_PERIOD) {
                getLastLocation()
            }
        }
    }

    private fun pausePolling() {
        timer?.cancel()
    }

    private fun initView() {
        loadingIndicator = findViewById(R.id.loading_indicator)
        loadingIndicator.visibility = View.GONE

        val english = getString(R.string.english)
        val french = getString(R.string.frensh)
        langChangeButton = findViewById(R.id.lang_change_button)
        langChangeButton.text = if (getCurrentLanguahe() == english) french else english
        langChangeButton.setOnClickListener {
            changeLanguage()
        }

        searchEditText = findViewById(R.id.search_query_edit_text)
        searchDistanceEditText = findViewById(R.id.distance_edit_text)
        searchDistanceEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validateDistance()
            }
        })

        searchButton = findViewById(R.id.search_button)
        searchButton.setOnClickListener {
            onUpdateSearchClicked()
        }

        tweetPopup = findViewById<View>(R.id.tweet_popup)
        tweetShortText = findViewById(R.id.tweet_text_view)
        tweetCreateDate = findViewById(R.id.tweet_date_text_view)
        moreButton = findViewById(R.id.more_button)
        moreButton.setOnClickListener {
            selectedTweet?.let { tweet ->
                TweetDetailedActivity.startTweetDetailedActivity(this, tweet)
            }
        }
    }

    private fun validateDistance() {
        try {
            val distance = searchDistanceEditText.text.toString().toDouble()
            if (distance > MAX_ALLOWED_DISTANCE) { //Max allowed
                Toast.makeText(
                    this@TwitterMapActivity,
                    getString(R.string.exceed_allowed_distance),
                    Toast.LENGTH_LONG
                ).show()
                searchDistanceEditText.setText("$MAX_ALLOWED_DISTANCE")
            }
        } catch (e: java.lang.Exception) {
        }
    }

    private fun onUpdateSearchClicked() {
        pausePolling()

        query = searchEditText.text.toString()
        radius = DEFAULT_DISTANCE
        try {
            radius = searchDistanceEditText.text.toString().toDouble()
            if (radius > MAX_ALLOWED_DISTANCE) {
                radius = MAX_ALLOWED_DISTANCE
            }
        } catch (e: Exception) {
        }

        for (marker in markerTweetMap.keys) {
            marker.remove()
        }
        markerTweetMap.clear()
        tweetIDList.clear()

        mCircle?.remove()
        animateCamera = true

        hideKeyBoard()

        Handler().postDelayed({ startPolling() }, 300)
    }

    private fun onMapMarkerClicked(marker: Marker?) {
        selectedTweet = markerTweetMap[marker]
        tweetShortText.text = selectedTweet?.text ?: ""
        tweetCreateDate.text = DateUtils.getCorrectedDateString(selectedTweet?.createdAt ?: "")

        hideKeyBoard()

        if (!isPopupDisplayed) {
            isPopupDisplayed = true
            UiUtils.slideUp(tweetPopup)
        }

        prevSelectedMarker?.zIndex = 0f
        prevSelectedMarker?.setIcon(
            BitmapDescriptorFactory.defaultMarker(
                BitmapDescriptorFactory.HUE_RED
            )
        )

        marker?.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
        marker?.zIndex = 1f

        prevSelectedMarker = marker
    }

    private fun onMarkerInfoWindowClosed(marker: Marker?) {
        marker?.hideInfoWindow()
        marker?.zIndex = 0f
        marker?.setIcon(
            BitmapDescriptorFactory.defaultMarker(
                BitmapDescriptorFactory.HUE_RED
            )
        )
        prevSelectedMarker = null

        Handler().postDelayed({
            if (prevSelectedMarker == null) {
                UiUtils.slideDown(tweetPopup)
                isPopupDisplayed = false
            }
        }, 200)
    }

    private fun requestTweets(lat: Double, lon: Double) {
        presenter.requestTweets(this, query, radius, lat, lon, this)
    }

    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    val location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        requestTweets(location.latitude, location.longitude)
                    }
                }
            } else {
                Toast.makeText(this, getString(R.string.enable_location), Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            PERMISSION_ID
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    override fun onGettingTweetsSuccess(
        tweetData: TweetData,
        lat: Double,
        lng: Double,
        radius: Double
    ) {
        if (tweetData.result != null) {
            for (tweet in tweetData.result!!) {
                val id: String = tweet.idStr ?: continue
                if (tweetIDList.contains(id)) {
                    continue
                }

                var tweetLat =
                    lat * Random.nextDouble(1.0, 1.00001)//randomize pins without exact location
                var tweetLon =
                    lng * Random.nextDouble(1.0, 1.00001)//randomize pins without exact location
                if (tweet.geo != null && tweet.geo!!.coordinates != null) {
                    tweetLat = tweet.geo!!.coordinates!![0].toDouble()
                    tweetLon = tweet.geo!!.coordinates!![1].toDouble()
                }
                val latLng = LatLng(tweetLat, tweetLon)
                val marker =
                    mMap.addMarker(MarkerOptions().position(latLng))

                markerTweetMap[marker] = tweet
                tweetIDList.add(id)
            }
        }

        if (markerTweetMap.size > MAX_ALLOWED_PINS) {
            val map = markerTweetMap.toSortedMap(compareBy {
                DateUtils.parse(
                    markerTweetMap[it]?.createdAt ?: ""
                )
            })

            while (markerTweetMap.size > MAX_ALLOWED_PINS && map.isNotEmpty()) {
                val marker = map.firstKey()
                map.remove(marker)
                val tweet = markerTweetMap.remove(marker)
                marker.remove()
                tweetIDList.remove(tweet?.idStr)
            }
        }

        if (markerTweetMap.size == 0) {
            Toast.makeText(this, getString(R.string.no_tweets_fetched), Toast.LENGTH_LONG)
                .show()
        }

        mCircle?.remove()
        val currentLocation = LatLng(lat, lng)
        val circleOptions = CircleOptions()
        circleOptions.center(currentLocation)
        circleOptions.radius(radius * 1000) //km to meter
        circleOptions.strokeColor(Color.RED)
        circleOptions.fillColor(0x20ff0000)
        circleOptions.strokeWidth(3f)
        mCircle = mMap.addCircle(circleOptions)

        if (animateCamera) {
            animateCamera = false
            mMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    circleOptions.center, getZoomLevel(mCircle)
                )
            )
        }

        Log.v("bell", "success")
    }

    private fun getZoomLevel(circle: Circle?): Float {
        var zoomLevel = 11
        if (circle != null) {
            val radius = circle.radius + circle.radius / 2
            val scale = radius / 500
            zoomLevel = (16 - ln(scale) / ln(2.0)).toInt()
        }
        return zoomLevel.toFloat()
    }

    override fun onGettingTweetsFailed() {
        Toast.makeText(this, getString(R.string.error_fetching_tweets), Toast.LENGTH_LONG)
            .show()
    }

    override fun setLoading(isLoading: Boolean) {
        loadingIndicator.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private fun hideKeyBoard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(searchEditText.windowToken, 0)
    }
}
