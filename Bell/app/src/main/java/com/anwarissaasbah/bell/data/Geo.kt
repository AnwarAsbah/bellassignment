package com.anwarissaasbah.bell.data

import com.fasterxml.jackson.annotation.*
import java.io.Serializable
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class Geo : Serializable {

    @JsonProperty("type")
    @get:JsonProperty("type")
    @set:JsonProperty("type")
    var type: String? = null
    @JsonProperty("coordinates")
    @get:JsonProperty("coordinates")
    @set:JsonProperty("coordinates")
    var coordinates: List<Float>? = null
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}
