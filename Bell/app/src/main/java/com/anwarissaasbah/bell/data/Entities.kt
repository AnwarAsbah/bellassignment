package com.anwarissaasbah.bell.data

import com.fasterxml.jackson.annotation.*
import java.io.Serializable
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class Entities : Serializable {

    @JsonProperty("media")
    @get:JsonProperty("media")
    @set:JsonProperty("media")
    var media: List<Media>? = null
    @JsonProperty("hashtags")
    @get:JsonProperty("hashtags")
    @set:JsonProperty("hashtags")
    var hashtags: List<Any>? = null
    @JsonProperty("urls")
    @get:JsonProperty("urls")
    @set:JsonProperty("urls")
    var urls: List<Url>? = null
    @JsonProperty("user_mentions")
    @get:JsonProperty("user_mentions")
    @set:JsonProperty("user_mentions")
    var userMentions: List<Any>? = null
    @JsonProperty("symbols")
    @get:JsonProperty("symbols")
    @set:JsonProperty("symbols")
    var symbols: List<Any>? = null
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}
