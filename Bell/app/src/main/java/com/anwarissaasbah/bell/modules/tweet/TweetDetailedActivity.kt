package com.anwarissaasbah.bell.modules.tweet

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import android.widget.VideoView
import com.anwarissaasbah.bell.BaseActivity
import com.anwarissaasbah.bell.R
import com.anwarissaasbah.bell.asynctasks.ImageUrlLoader
import com.anwarissaasbah.bell.data.Tweet
import com.anwarissaasbah.bell.utils.DateUtils
import com.anwarissaasbah.bell.utils.UiUtils

private const val MODEL_ARG = "MODEL_ARG"

private const val PHOTO = "photo"
private const val GIF = "animated_gif"
private const val VIDEO = "video"
private const val MP4 = "video/mp4"

class TweetDetailedActivity : BaseActivity(), TweetPresenter.TwitterTweetView {

    private lateinit var backButton: View
    private lateinit var userAvatar: ImageView
    private lateinit var userNameTextView: TextView
    private lateinit var followingIcon: ImageView
    private lateinit var userTwitterNameTextView: TextView
    private lateinit var createdAtTextView: TextView
    private lateinit var msgTextView: TextView
    private lateinit var tweetImageView: ImageView
    private lateinit var tweetVideoView: VideoView
    private lateinit var retweetButton: View
    private lateinit var favoriteButton: View
    private lateinit var favoriteImageView: ImageView

    private var tweetModel: Tweet? = null
    private val presenter: TweetPresenter = TweetPresenter()

    companion object {
        fun startTweetDetailedActivity(context: Context, tweet: Tweet) {
            val tweetDetailedActivityIntent = Intent(context, TweetDetailedActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(MODEL_ARG, tweet)
            tweetDetailedActivityIntent.putExtras(bundle)
            context.startActivity(tweetDetailedActivityIntent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tweet_details)

        readArgs()
        init()
        bindModel()
    }

    private fun readArgs() {
        val bundle = intent.extras
        if (bundle != null && bundle.containsKey(MODEL_ARG)) {
            tweetModel = (bundle.getSerializable(MODEL_ARG)
                ?: throw IllegalStateException("field $MODEL_ARG missing in Intent")) as Tweet?
        } else {
            throw IllegalStateException("field $MODEL_ARG missing in Intent")
        }
    }

    private fun init() {
        userAvatar = findViewById(R.id.user_avatar)
        userNameTextView = findViewById(R.id.user_name_text_view)
        followingIcon = findViewById(R.id.following_icon)
        userTwitterNameTextView = findViewById(R.id.user_id_text_view)
        createdAtTextView = findViewById(R.id.created_at_text_view)

        msgTextView = findViewById(R.id.msg_text_view)
        msgTextView.movementMethod = LinkMovementMethod.getInstance()

        tweetImageView = findViewById(R.id.tweet_image_view)
        tweetVideoView = findViewById(R.id.tweet_video_view)

        retweetButton = findViewById(R.id.retweet_button)
        retweetButton.setOnClickListener {
            retweet()
        }

        favoriteImageView = findViewById(R.id.favorite_image_view)
        favoriteButton = findViewById(R.id.favorite_button)
        favoriteButton.setOnClickListener {
            like()
        }

        backButton = findViewById(R.id.back_button)
        backButton.setOnClickListener {
            onBackPressed()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun bindModel() {
        if (tweetModel == null) {
            return
        }

        if (tweetModel!!.user != null) {
            val user = tweetModel!!.user
            ImageUrlLoader().load(user!!.profileImageUrlHttps, userAvatar)
            userNameTextView.text = user.name
            followingIcon.visibility =
                if (user.following == null || !user.following!!) View.GONE else View.VISIBLE
            userTwitterNameTextView.text = "@${user.screenName}"
        }

        val tweetMainEntities = tweetModel!!.entities

        if (tweetModel!!.isTruncated && tweetModel!!.extendedTweet != null && tweetModel!!.extendedTweet!!.fullText != null) {
            //handling big msg
            val extendedTweet = tweetModel!!.extendedTweet!!
            val msg = extendedTweet.fullText
            val spannable = SpannableString(msg)

            val entities = extendedTweet.entities
            if (entities?.urls != null && entities.urls!!.isNotEmpty()) {
                for (url in extendedTweet.entities!!.urls!!) {
                    UiUtils.setHyperLink(this, spannable, url.url ?: "")
                }
            }
            msgTextView.text = spannable
        } else {
            //small msg
            val msg = tweetModel!!.text
            val spannable = SpannableString(msg)
            if (tweetMainEntities?.urls != null && tweetMainEntities.urls!!.isNotEmpty()) {
                for (url in tweetModel!!.entities!!.urls!!) {
                    UiUtils.setHyperLink(this, spannable, url.url ?: "")
                }
            }
            msgTextView.text = spannable
        }

        createdAtTextView.text = DateUtils.getCorrectedDateString(tweetModel!!.createdAt ?: "")

        if (tweetMainEntities?.media != null && tweetMainEntities.media!!.isNotEmpty()) {
            val media = tweetMainEntities.media!![0]

            if (media.type.equals(PHOTO, true)) {
                //photo
                tweetImageView.visibility = View.VISIBLE
                ImageUrlLoader().load(media.media_url_https, tweetImageView)
            } else if (media.type.equals(VIDEO, true) ||
                media.type.equals(MP4, true) ||
                media.type.equals(GIF, true)
            ) {
                //video
                if (media.videoInfo != null && media.videoInfo!!.variants != null && media.videoInfo!!.variants!!.isNotEmpty()) {
                    val videoUrl = media.videoInfo!!.variants!![0].url ?: ""
                    tweetVideoView.visibility = View.VISIBLE
                    tweetVideoView.setVideoPath(videoUrl)
                    tweetVideoView.start()
                    tweetVideoView.setOnCompletionListener {
                        tweetVideoView.start()
                    }
                }
            }
        }

        updateFavoriteIcon()
    }

    private fun updateFavoriteIcon() {
        val isLiked = tweetModel != null && tweetModel!!.isFavorited
        favoriteImageView.setImageResource(
            if (isLiked) R.drawable.favorite_heart_selected else
                R.drawable.favorite_heart
        )
    }

    private fun retweet() {
        presenter.retweet(this, tweetModel, this)
    }

    private fun like() {
        presenter.like(this, tweetModel, this)
    }

    override fun setFavoriteButtonEnabled(enabled: Boolean) {
        favoriteButton.isEnabled = enabled
    }

    override fun preFavoriteExec() {
        updateFavoriteIcon()
    }

    override fun onFavoriteFail() {
        tweetModel!!.isFavorited = !(tweetModel!!.isFavorited)
        updateFavoriteIcon()
        Toast.makeText(this, getString(R.string.fail), Toast.LENGTH_LONG).show()
    }

    override fun setRetweetingButtonEnabled(enabled: Boolean) {
        retweetButton.isEnabled = enabled
    }

    override fun onAlreadyTweeted() {
        Toast.makeText(this, getString(R.string.already_retweeted), Toast.LENGTH_LONG).show()
    }

    override fun onRetweetFail() {
        tweetModel!!.isRetweeted = false
        Toast.makeText(this, getString(R.string.fail), Toast.LENGTH_LONG).show()
    }

    override fun onSuccess() {
        Toast.makeText(this, getString(R.string.success), Toast.LENGTH_LONG).show()
    }
}
