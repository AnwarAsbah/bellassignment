package com.anwarissaasbah.bell.data

import com.fasterxml.jackson.annotation.*
import java.io.Serializable
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class Url : Serializable {

    @JsonProperty("url")
    @get:JsonProperty("url")
    @set:JsonProperty("url")
    var url: String? = null
    @JsonProperty("expanded_url")
    @get:JsonProperty("expanded_url")
    @set:JsonProperty("expanded_url")
    var expandedUrl: String? = null
    @JsonProperty("display_url")
    @get:JsonProperty("display_url")
    @set:JsonProperty("display_url")
    var displayUrl: String? = null
    @JsonProperty("indices")
    @get:JsonProperty("indices")
    @set:JsonProperty("indices")
    var indices: List<Int>? = null
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}
