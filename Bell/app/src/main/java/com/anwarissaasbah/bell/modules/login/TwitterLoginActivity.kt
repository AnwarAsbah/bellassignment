package com.anwarissaasbah.bell.modules.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.anwarissaasbah.bell.BaseActivity
import com.anwarissaasbah.bell.R
import com.anwarissaasbah.bell.modules.map.TwitterMapActivity
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterLoginButton

class TwitterLoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_twitter_login)

        val loginButton = findViewById<View>(R.id.twitter_login) as TwitterLoginButton
        loginButton.callback = object : Callback<TwitterSession>() {
            override fun failure(exception: TwitterException?) {
            }

            override fun success(result: Result<TwitterSession>?) {
                val session = TwitterCore.getInstance().sessionManager.activeSession
                val authToken = session.authToken
                val token = authToken.token
                val secret = authToken.secret

                if (token != null || secret != null) {
                    startTwitterMapActivity()
                }
            }
        }
    }

    private fun startTwitterMapActivity() {
        startActivity(Intent(this, TwitterMapActivity::class.java))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val loginButton = findViewById<View>(R.id.twitter_login) as TwitterLoginButton
        loginButton.onActivityResult(requestCode, resultCode, data)
    }
}
