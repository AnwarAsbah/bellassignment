package com.anwarissaasbah.bell.data

import com.fasterxml.jackson.annotation.*
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class Variant {

    @JsonProperty("bitrate")
    @get:JsonProperty("bitrate")
    @set:JsonProperty("bitrate")
    var bitrate: Int = 0
    @JsonProperty("content_type")
    @get:JsonProperty("content_type")
    @set:JsonProperty("content_type")
    var contentType: String? = null
    @JsonProperty("url")
    @get:JsonProperty("url")
    @set:JsonProperty("url")
    var url: String? = null
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}
