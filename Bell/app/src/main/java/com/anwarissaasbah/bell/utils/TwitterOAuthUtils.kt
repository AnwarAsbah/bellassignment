package com.anwarissaasbah.bell.utils

import java.net.URLEncoder
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import kotlin.random.Random

private const val HMAC_SHA1_ALGORITHM = "HmacSHA1"

private const val OAUTH_CONSUMER_KEY = "oauth_consumer_key"
private const val OAUTH_NONCE_KEY = "oauth_nonce"
private const val OAUTH_SIGNATURE_KEY = "oauth_signature"
private const val OAUTH_SIGNATURE_METHOD_KEY = "oauth_signature_method"
private const val OAUTH_TIMESTAMP_KEY = "oauth_timestamp"
private const val OAUTH_TOKEN_KEY = "oauth_token"
private const val OAUTH_VERSION_KEY = "oauth_version"

private const val OAUTH_SIGNATURE_METHOD_VALUE = "HMAC-SHA1"
private const val OAUTH_VERSION_VALUE = "1.0"

private const val SIGNATURE_PAIR_SEPARATOR = "&"
private const val SIGNATURE_KEY_VALUE_SEPARATOR = "="

private const val JAVA_SPACE_ENCODING = "+"
private const val HTML_SPACE_ENCODING = "%20"
private const val UTF8 = "UTF-8"
private const val OAuth = "OAuth "

object TwitterOAuthUtils {

    @JvmStatic
    fun getAuthorizationString(
        httpMethod: String, url: String, queryMap: HashMap<String, String>, consumerKey: String,
        consumerSecret: String, token: String, tokenSecret: String
    ): String {
        val nonceString = getRandomAlphaNumericKey()
        val timeStamp = System.currentTimeMillis() / 1000 //inSeconds

        val authorizeMap = LinkedHashMap<String, String>()
        authorizeMap[OAUTH_CONSUMER_KEY] = consumerKey
        authorizeMap[OAUTH_NONCE_KEY] = nonceString
        authorizeMap[OAUTH_SIGNATURE_KEY] = getSignature(
            httpMethod, url, queryMap, consumerKey, consumerSecret, token,
            tokenSecret, nonceString, timeStamp
        )
        authorizeMap[OAUTH_SIGNATURE_METHOD_KEY] = OAUTH_SIGNATURE_METHOD_VALUE
        authorizeMap[OAUTH_TIMESTAMP_KEY] = timeStamp.toString()
        authorizeMap[OAUTH_TOKEN_KEY] = token
        authorizeMap[OAUTH_VERSION_KEY] = OAUTH_VERSION_VALUE

        var index = 0
        var authorizeString = OAuth
        for (key in authorizeMap.keys) {
            authorizeString += URLEncoder.encode(key, UTF8)
            authorizeString += "=\"" + URLEncoder.encode(authorizeMap[key], UTF8) + "\""
            authorizeString += if (index == authorizeMap.size - 1) "" else ", "
            index += 1
        }

        return authorizeString.replace(JAVA_SPACE_ENCODING, HTML_SPACE_ENCODING)
    }

    @JvmStatic
    private fun getSignature(
        httpMethod: String, url: String, queryMap: HashMap<String, String>, consumerKey: String,
        consumerSecret: String, token: String, tokenSecret: String, oauth_nonce: String,
        timeStamp: Long
    ): String {

        val paramsMap = HashMap<String, String>(queryMap)
        paramsMap[OAUTH_CONSUMER_KEY] = consumerKey
        paramsMap[OAUTH_NONCE_KEY] = oauth_nonce
        paramsMap[OAUTH_SIGNATURE_METHOD_KEY] = OAUTH_SIGNATURE_METHOD_VALUE
        paramsMap[OAUTH_TIMESTAMP_KEY] = timeStamp.toString()
        paramsMap[OAUTH_TOKEN_KEY] = token
        paramsMap[OAUTH_VERSION_KEY] = OAUTH_VERSION_VALUE

        val sortedParams = paramsMap.toSortedMap()

        var param = ""
        var keyIndex = 0
        for (key in sortedParams.keys) {

            param += URLEncoder.encode(key, UTF8) +
                    SIGNATURE_KEY_VALUE_SEPARATOR +
                    URLEncoder.encode(sortedParams[key], UTF8) +
                    (if (keyIndex == sortedParams.size - 1) "" else SIGNATURE_PAIR_SEPARATOR)

            keyIndex += 1
        }
        //space should be %20 not +
        param = param.replace(JAVA_SPACE_ENCODING, HTML_SPACE_ENCODING)

        val rawSig = httpMethod.toUpperCase() +
                SIGNATURE_PAIR_SEPARATOR +
                URLEncoder.encode(url, UTF8) +
                SIGNATURE_PAIR_SEPARATOR +
                URLEncoder.encode(param, UTF8)


        val signingKeyString = URLEncoder.encode(consumerSecret, UTF8) +
                SIGNATURE_PAIR_SEPARATOR +
                URLEncoder.encode(tokenSecret, UTF8)

        val signingKey = SecretKeySpec(
            signingKeyString.toByteArray(),
            HMAC_SHA1_ALGORITHM
        )
        val mac = Mac.getInstance(HMAC_SHA1_ALGORITHM)
        mac.init(signingKey)
        val signature = String(
            android.util.Base64.encode(
                mac.doFinal(rawSig.toByteArray()),
                android.util.Base64.CRLF
            )
        )

        return signature.replace("\r", "").replace("\n", "")
    }

    @JvmStatic
    private fun getRandomAlphaNumericKey(): String {

        var randomAlphaNumericKey = ""
        for (i in 1..40) {
            randomAlphaNumericKey += when (Random.nextInt(3)) {
                0 ->
                    ('a' + Random.nextInt(26))
                1 ->
                    ('A' + Random.nextInt(26))
                else ->
                    ('0' + Random.nextInt(10))
            }
        }

        return randomAlphaNumericKey
    }

    @JvmStatic
    fun buildUrl(url: String, queryMap: HashMap<String, String>): String {
        var fullUrl = "$url?"
        var keyIndex = 0
        for (key in queryMap.keys) {
            fullUrl += "$key=" + URLEncoder.encode(queryMap[key], UTF8) +
                    (if (keyIndex == queryMap.size - 1) "" else SIGNATURE_PAIR_SEPARATOR)
            keyIndex += 1
        }

        return fullUrl.replace(JAVA_SPACE_ENCODING, HTML_SPACE_ENCODING)
    }
}