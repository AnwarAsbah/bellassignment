import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Build
import com.anwarissaasbah.bell.R
import com.anwarissaasbah.bell.utils.PreferenceHelper
import java.util.*

const val LANG_KEY: String = "land_key"

object LocaleManager {

    var mSharedPreference: SharedPreferences? = null

    fun setLocale(context: Context?): Context {
        return updateResources(context!!, getCurrentLanguage(context)!!)
    }

    inline fun setNewLocale(context: Context, language: String) {

        persistLanguagePreference(context, language)
        updateResources(context, language)
    }

    inline fun getCurrentLanguage(context: Context?): String? {

        var mCurrentLanguage: String?

        if (mSharedPreference == null)
            mSharedPreference = PreferenceHelper.defaultPrefs(context!!)


        mCurrentLanguage = PreferenceHelper.getString(
            mSharedPreference!!,
            LANG_KEY,
            context?.getString(R.string.english) ?: ""
        )

        return mCurrentLanguage
    }

    fun persistLanguagePreference(context: Context, language: String) {
        if (mSharedPreference == null)
            mSharedPreference = PreferenceHelper.defaultPrefs(context)

        PreferenceHelper.writeString(mSharedPreference!!, LANG_KEY, language)
    }

    fun updateResources(context: Context, language: String): Context {

        var contextFun = context

        var locale = Locale(language)
        Locale.setDefault(locale)

        var resources = context.resources
        var configuration = Configuration(resources.configuration)

        if (Build.VERSION.SDK_INT >= 17) {
            configuration.setLocale(locale)
            contextFun = context.createConfigurationContext(configuration)
        } else {
            configuration.locale = locale
            resources.updateConfiguration(configuration, resources.getDisplayMetrics())
        }
        return contextFun
    }
}