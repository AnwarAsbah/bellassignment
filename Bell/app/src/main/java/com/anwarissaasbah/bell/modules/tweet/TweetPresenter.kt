package com.anwarissaasbah.bell.modules.tweet

import android.content.Context
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.anwarissaasbah.bell.BellApp
import com.anwarissaasbah.bell.R
import com.anwarissaasbah.bell.data.Tweet
import com.anwarissaasbah.bell.utils.TwitterOAuthUtils
import com.twitter.sdk.android.core.TwitterCore

private const val RETWEET_API = "https://api.twitter.com/1.1/statuses/retweet/%s.json"
private const val LIKE_API = "https://api.twitter.com/1.1/favorites/create.json"
private const val UNLIKE_API = "https://api.twitter.com/1.1/favorites/destroy.json"

class TweetPresenter {

    fun retweet(context: Context, tweetModel: Tweet?, view: TwitterTweetView) {
        if (tweetModel != null) {
            if (tweetModel.isRetweeted) {
                view.onAlreadyTweeted()
                return
            }

            tweetModel.isRetweeted = true
            view.setRetweetingButtonEnabled(false)
            val url = RETWEET_API.format(tweetModel.idStr)

            val jsonObjectRequest = object : JsonObjectRequest(
                Method.POST, url, null,
                Response.Listener {
                    view.setRetweetingButtonEnabled(true)
                    view.onSuccess()
                },
                Response.ErrorListener {
                    view.setRetweetingButtonEnabled(true)
                    view.onRetweetFail()
                }) {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val params = HashMap(super.getHeaders())
                    val token =
                        TwitterCore.getInstance().sessionManager.activeSession.authToken.token
                    val tokenSecret =
                        TwitterCore.getInstance().sessionManager.activeSession.authToken.secret
                    val authorizeString = TwitterOAuthUtils.getAuthorizationString(
                        "POST",
                        url, HashMap(),
                        context.getString(R.string.com_twitter_sdk_android_CONSUMER_KEY),
                        context.getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET),
                        token, tokenSecret
                    )
                    params["Authorization"] = authorizeString
                    return params
                }
            }

            BellApp.getRequestQueue().add(jsonObjectRequest)
        }
    }

    fun like(context: Context, tweetModel: Tweet?, view: TwitterTweetView) {
        if (tweetModel != null) {
            view.setFavoriteButtonEnabled(false)

            val isLike = !(tweetModel.isFavorited)
            tweetModel.isFavorited = isLike
            view.preFavoriteExec()

            val param = LinkedHashMap<String, String>()
            param["id"] = tweetModel.idStr ?: ""

            val url = if (isLike) LIKE_API else UNLIKE_API
            val fullUrl = TwitterOAuthUtils.buildUrl(url, param)

            val jsonObjectRequest = object : JsonObjectRequest(
                Method.POST, fullUrl, null,
                Response.Listener {
                    view.setFavoriteButtonEnabled(true)
                    view.onSuccess()
                },
                Response.ErrorListener {
                    view.setFavoriteButtonEnabled(true)
                    view.onFavoriteFail()
                }) {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val params = HashMap(super.getHeaders())
                    val token =
                        TwitterCore.getInstance().sessionManager.activeSession.authToken.token
                    val tokenSecret =
                        TwitterCore.getInstance().sessionManager.activeSession.authToken.secret
                    val authorizeString = TwitterOAuthUtils.getAuthorizationString(
                        "POST",
                        url, param,
                        context.getString(R.string.com_twitter_sdk_android_CONSUMER_KEY),
                        context.getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET),
                        token, tokenSecret
                    )
                    params["Authorization"] = authorizeString
                    return params
                }
            }

            BellApp.getRequestQueue().add(jsonObjectRequest)
        }
    }

    interface TwitterTweetView {
        fun onAlreadyTweeted()
        fun setRetweetingButtonEnabled(enabled: Boolean)
        fun setFavoriteButtonEnabled(enabled: Boolean)
        fun preFavoriteExec()
        fun onSuccess()
        fun onRetweetFail()
        fun onFavoriteFail()
    }
}