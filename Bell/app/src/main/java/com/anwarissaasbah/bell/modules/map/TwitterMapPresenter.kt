package com.anwarissaasbah.bell.modules.map

import TweetData
import android.content.Context
import android.util.Log
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.anwarissaasbah.bell.BellApp
import com.anwarissaasbah.bell.R
import com.anwarissaasbah.bell.utils.TwitterOAuthUtils
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.twitter.sdk.android.core.TwitterCore

private const val GET_HTTP_METHOD = "GET"
private const val SERVICE_URL = "https://api.twitter.com/1.1/search/tweets.json"

class TwitterMapPresenter {

    fun requestTweets(
        context: Context,
        query: String,
        radius: Double,
        lat: Double,
        lon: Double,
        view: TwitterMapView
    ) {

        view.setLoading(true)

        val queryParams = LinkedHashMap<String, String>()
        queryParams["q"] = query//"$query (point_radius:[$lon $lat ${radius}km])"
        queryParams["geocode"] = "$lat,$lon,${radius}km"
        queryParams["result_type"] = "recent"
        queryParams["count"] = "100"

        val url = TwitterOAuthUtils.buildUrl(SERVICE_URL, queryParams)
        Log.v("anwarTest", "url $url")

        val stringRequest: Request<String> = object : StringRequest(
            Method.GET, url,
            Response.Listener { response ->

                view.setLoading(false)

                val mapper = jacksonObjectMapper()
                ObjectMapper().registerModule(KotlinModule())
                val tweetData = mapper.readValue<TweetData>(response)

                view.onGettingTweetsSuccess(tweetData, lat, lon, radius)
            },
            Response.ErrorListener {
                view.setLoading(false)
                view.onGettingTweetsFailed()
            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val params = HashMap(super.getHeaders())
                val token = TwitterCore.getInstance().sessionManager.activeSession.authToken.token
                val tokenSecret =
                    TwitterCore.getInstance().sessionManager.activeSession.authToken.secret
                val authorizeString = TwitterOAuthUtils.getAuthorizationString(
                    GET_HTTP_METHOD,
                    SERVICE_URL, queryParams,
                    context.getString(R.string.com_twitter_sdk_android_CONSUMER_KEY),
                    context.getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET),
                    token, tokenSecret
                )
                Log.v("anwarTest", "authorizeString $authorizeString")
                params["Authorization"] = authorizeString
                return params
            }
        }

        BellApp.getRequestQueue().add(stringRequest)
    }

    interface TwitterMapView {
        fun setLoading(isLoading: Boolean)

        fun onGettingTweetsFailed()

        fun onGettingTweetsSuccess(tweetData: TweetData, lat: Double, lng: Double, radius: Double)
    }
}