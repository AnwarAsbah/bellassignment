package com.anwarissaasbah.bell

import LANG_KEY
import LocaleManager
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.anwarissaasbah.bell.utils.PreferenceHelper


abstract class BaseActivity : AppCompatActivity() {

    var mSharedPreference: SharedPreferences? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleManager.setLocale(base))
    }

    fun changeLanguage() {
        var languageToLoad = getCurrentLanguahe()
        languageToLoad = if (languageToLoad == getString(R.string.english))
            getString(R.string.frensh) else getString(R.string.english)

        LocaleManager.setNewLocale(this, languageToLoad)

        val intent = intent
        finish()
        startActivity(intent)
    }

    fun getCurrentLanguahe(): String {
        if (mSharedPreference == null) {
            mSharedPreference = PreferenceHelper.defaultPrefs(this)
        }

        return PreferenceHelper.getString(
            mSharedPreference!!,
            LANG_KEY,
            getString(R.string.english)
        )
    }
}