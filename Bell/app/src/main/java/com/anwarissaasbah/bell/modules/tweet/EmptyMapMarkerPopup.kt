package com.anwarissaasbah.bell.modules.tweet

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.anwarissaasbah.bell.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

//wrapper class so transparent InfoWindow view
public class EmptyMapMarkerPopup(context: Context) : GoogleMap.InfoWindowAdapter {

    private val markerPopup: View =
        LayoutInflater.from(context).inflate(R.layout.tweet_map_marker, null)

    override fun getInfoContents(p0: Marker?): View? {
        return null
    }

    override fun getInfoWindow(p0: Marker?): View? {
        return markerPopup
    }
}