package com.anwarissaasbah.bell.data

import com.fasterxml.jackson.annotation.*
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class VideoInfo {

    @JsonProperty("aspect_ratio")
    @get:JsonProperty("aspect_ratio")
    @set:JsonProperty("aspect_ratio")
    var aspectRatio: List<Int>? = null
    @JsonProperty("duration_millis")
    @get:JsonProperty("duration_millis")
    @set:JsonProperty("duration_millis")
    var durationMillis: Int = 0
    @JsonProperty("variants")
    @get:JsonProperty("variants")
    @set:JsonProperty("variants")
    var variants: List<Variant>? = null
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}
