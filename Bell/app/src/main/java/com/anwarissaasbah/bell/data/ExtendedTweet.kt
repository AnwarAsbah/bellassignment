package com.anwarissaasbah.bell.data

import com.fasterxml.jackson.annotation.*
import java.io.Serializable
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class ExtendedTweet : Serializable {

    @JsonProperty("full_text")
    @get:JsonProperty("full_text")
    @set:JsonProperty("full_text")
    var fullText: String? = null
    @JsonProperty("entities")
    @get:JsonProperty("entities")
    @set:JsonProperty("entities")
    var entities: Entities? = null
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}