package com.anwarissaasbah.bell.data

import com.fasterxml.jackson.annotation.*
import java.io.Serializable
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class User : Serializable {

    @JsonProperty("id")
    @get:JsonProperty("id")
    @set:JsonProperty("id")
    var id: Long = 0
    @JsonProperty("id_str")
    @get:JsonProperty("id_str")
    @set:JsonProperty("id_str")
    var idStr: String? = null
    @JsonProperty("name")
    @get:JsonProperty("name")
    @set:JsonProperty("name")
    var name: String? = null
    @JsonProperty("screen_name")
    @get:JsonProperty("screen_name")
    @set:JsonProperty("screen_name")
    var screenName: String? = null
    @JsonProperty("location")
    @get:JsonProperty("location")
    @set:JsonProperty("location")
    var location: String? = null
    @JsonProperty("url")
    @get:JsonProperty("url")
    @set:JsonProperty("url")
    var url: Any? = null
    @JsonProperty("description")
    @get:JsonProperty("description")
    @set:JsonProperty("description")
    var description: String? = null
    @JsonProperty("translator_type")
    @get:JsonProperty("translator_type")
    @set:JsonProperty("translator_type")
    var translatorType: String? = null
    @JsonProperty("protected")
    @get:JsonProperty("protected")
    @set:JsonProperty("protected")
    var isProtected: Boolean = false
    @JsonProperty("verified")
    @get:JsonProperty("verified")
    @set:JsonProperty("verified")
    var isVerified: Boolean = false
    @JsonProperty("followers_count")
    @get:JsonProperty("followers_count")
    @set:JsonProperty("followers_count")
    var followersCount: Int = 0
    @JsonProperty("friends_count")
    @get:JsonProperty("friends_count")
    @set:JsonProperty("friends_count")
    var friendsCount: Int = 0
    @JsonProperty("listed_count")
    @get:JsonProperty("listed_count")
    @set:JsonProperty("listed_count")
    var listedCount: Int = 0
    @JsonProperty("favourites_count")
    @get:JsonProperty("favourites_count")
    @set:JsonProperty("favourites_count")
    var favouritesCount: Int = 0
    @JsonProperty("statuses_count")
    @get:JsonProperty("statuses_count")
    @set:JsonProperty("statuses_count")
    var statusesCount: Int = 0
    @JsonProperty("created_at")
    @get:JsonProperty("created_at")
    @set:JsonProperty("created_at")
    var createdAt: String? = null
    @JsonProperty("utc_offset")
    @get:JsonProperty("utc_offset")
    @set:JsonProperty("utc_offset")
    var utcOffset: Any? = null
    @JsonProperty("time_zone")
    @get:JsonProperty("time_zone")
    @set:JsonProperty("time_zone")
    var timeZone: Any? = null
    @JsonProperty("geo_enabled")
    @get:JsonProperty("geo_enabled")
    @set:JsonProperty("geo_enabled")
    var isGeoEnabled: Boolean = false
    @JsonProperty("lang")
    @get:JsonProperty("lang")
    @set:JsonProperty("lang")
    var lang: Any? = null
    @JsonProperty("contributors_enabled")
    @get:JsonProperty("contributors_enabled")
    @set:JsonProperty("contributors_enabled")
    var isContributorsEnabled: Boolean = false
    @JsonProperty("is_translator")
    @get:JsonProperty("is_translator")
    @set:JsonProperty("is_translator")
    var isIsTranslator: Boolean = false
    @JsonProperty("profile_background_color")
    @get:JsonProperty("profile_background_color")
    @set:JsonProperty("profile_background_color")
    var profileBackgroundColor: String? = null
    @JsonProperty("profile_background_image_url")
    @get:JsonProperty("profile_background_image_url")
    @set:JsonProperty("profile_background_image_url")
    var profileBackgroundImageUrl: String? = null
    @JsonProperty("profile_background_image_url_https")
    @get:JsonProperty("profile_background_image_url_https")
    @set:JsonProperty("profile_background_image_url_https")
    var profileBackgroundImageUrlHttps: String? = null
    @JsonProperty("profile_background_tile")
    @get:JsonProperty("profile_background_tile")
    @set:JsonProperty("profile_background_tile")
    var isProfileBackgroundTile: Boolean = false
    @JsonProperty("profile_link_color")
    @get:JsonProperty("profile_link_color")
    @set:JsonProperty("profile_link_color")
    var profileLinkColor: String? = null
    @JsonProperty("profile_sidebar_border_color")
    @get:JsonProperty("profile_sidebar_border_color")
    @set:JsonProperty("profile_sidebar_border_color")
    var profileSidebarBorderColor: String? = null
    @JsonProperty("profile_sidebar_fill_color")
    @get:JsonProperty("profile_sidebar_fill_color")
    @set:JsonProperty("profile_sidebar_fill_color")
    var profileSidebarFillColor: String? = null
    @JsonProperty("profile_text_color")
    @get:JsonProperty("profile_text_color")
    @set:JsonProperty("profile_text_color")
    var profileTextColor: String? = null
    @JsonProperty("profile_use_background_image")
    @get:JsonProperty("profile_use_background_image")
    @set:JsonProperty("profile_use_background_image")
    var isProfileUseBackgroundImage: Boolean = false
    @JsonProperty("profile_image_url")
    @get:JsonProperty("profile_image_url")
    @set:JsonProperty("profile_image_url")
    var profileImageUrl: String? = null
    @JsonProperty("profile_image_url_https")
    @get:JsonProperty("profile_image_url_https")
    @set:JsonProperty("profile_image_url_https")
    var profileImageUrlHttps: String? = null
    @JsonProperty("profile_banner_url")
    @get:JsonProperty("profile_banner_url")
    @set:JsonProperty("profile_banner_url")
    var profileBannerUrl: String? = null
    @JsonProperty("default_profile")
    @get:JsonProperty("default_profile")
    @set:JsonProperty("default_profile")
    var isDefaultProfile: Boolean = false
    @JsonProperty("default_profile_image")
    @get:JsonProperty("default_profile_image")
    @set:JsonProperty("default_profile_image")
    var isDefaultProfileImage: Boolean = false
    @JsonProperty("following")
    @get:JsonProperty("following")
    @set:JsonProperty("following")
    var following: Boolean? = null
    @JsonProperty("follow_request_sent")
    @get:JsonProperty("follow_request_sent")
    @set:JsonProperty("follow_request_sent")
    var followRequestSent: Any? = null
    @JsonProperty("notifications")
    @get:JsonProperty("notifications")
    @set:JsonProperty("notifications")
    var notifications: Any? = null
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}
