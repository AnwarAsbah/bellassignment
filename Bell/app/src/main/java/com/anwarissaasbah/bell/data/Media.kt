package com.anwarissaasbah.bell.data

import com.fasterxml.jackson.annotation.*
import java.io.Serializable
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class Media : Serializable {

    @JsonProperty("type")
    @get:JsonProperty("type")
    @set:JsonProperty("type")
    var type: String? = "video"
    @JsonProperty("media_url_https")
    @get:JsonProperty("media_url_https")
    @set:JsonProperty("media_url_https")
    var media_url_https: String? = "https://pbs.twimg.com/media/EIiTzUUXYAAP9DQ.jpg"
    @JsonProperty("video_info")
    @get:JsonProperty("video_info")
    @set:JsonProperty("video_info")
    var videoInfo: VideoInfo? = null
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}
