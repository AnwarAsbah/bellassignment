import com.anwarissaasbah.bell.data.Tweet
import com.fasterxml.jackson.annotation.*
import java.io.Serializable
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class TweetData : Serializable {

    @JsonProperty("statuses")
    @get:JsonProperty("statuses")
    @set:JsonProperty("statuses")
    var result: LinkedList<Tweet>? = null

    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}