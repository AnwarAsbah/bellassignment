package com.anwarissaasbah.bell.asynctasks

import android.graphics.Bitmap
import android.widget.ImageView
import com.android.volley.Response
import com.android.volley.toolbox.ImageRequest
import com.anwarissaasbah.bell.BellApp


class ImageUrlLoader {

    fun load(url: String?, imageView: ImageView) {
        if (url == null) {
            return
        }

        val imageRequest = ImageRequest(
            url,
            Response.Listener { response ->
                imageView.setImageBitmap(response)
            },
            0,
            0,
            ImageView.ScaleType.FIT_CENTER,
            Bitmap.Config.RGB_565,
            Response.ErrorListener { error ->
            }
        )

        BellApp.getRequestQueue().add(imageRequest)
    }
}