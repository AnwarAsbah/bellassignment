package com.anwarissaasbah.bell

import LocaleManager
import android.app.Application
import android.content.Context
import android.content.res.Configuration
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.twitter.sdk.android.core.Twitter


class BellApp : Application() {

    companion object {
        private lateinit var queue: RequestQueue
        lateinit var instance: BellApp

        fun getRequestQueue(): RequestQueue {
            return queue
        }
    }

    override fun onCreate() {
        super.onCreate()
        Twitter.initialize(this)

        instance = this
        queue = Volley.newRequestQueue(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LocaleManager.setLocale(this)
    }
}