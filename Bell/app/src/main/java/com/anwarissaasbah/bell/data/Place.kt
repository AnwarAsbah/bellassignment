package com.anwarissaasbah.bell.data

import com.fasterxml.jackson.annotation.*
import java.io.Serializable
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class Place : Serializable {

    @JsonProperty("id")
    @get:JsonProperty("id")
    @set:JsonProperty("id")
    var id: String? = null
    @JsonProperty("url")
    @get:JsonProperty("url")
    @set:JsonProperty("url")
    var url: String? = null
    @JsonProperty("place_type")
    @get:JsonProperty("place_type")
    @set:JsonProperty("place_type")
    var placeType: String? = null
    @JsonProperty("name")
    @get:JsonProperty("name")
    @set:JsonProperty("name")
    var name: String? = null
    @JsonProperty("full_name")
    @get:JsonProperty("full_name")
    @set:JsonProperty("full_name")
    var fullName: String? = null
    @JsonProperty("country_code")
    @get:JsonProperty("country_code")
    @set:JsonProperty("country_code")
    var countryCode: String? = null
    @JsonProperty("country")
    @get:JsonProperty("country")
    @set:JsonProperty("country")
    var country: String? = null
    @JsonProperty("bounding_box")
    @get:JsonProperty("bounding_box")
    @set:JsonProperty("bounding_box")
    var boundingBox: BoundingBox? = null
    @JsonProperty("attributes")
    @get:JsonProperty("attributes")
    @set:JsonProperty("attributes")
    var attributes: Attributes? = null
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}
